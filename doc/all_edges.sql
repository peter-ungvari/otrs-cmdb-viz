select c1.id as source_id, t.id as link_type_id, c2.id as target_id,
  c1.configitem_number as source_number, c2.configitem_number as target_number,
  v1.name as source_name, t.name as link_type_name, v2.name as target_name
from link_relation r
join link_type t on r.type_id = t.id
join link_object o on o.name = 'ITSMConfigItem' and o.id = r.source_object_id and o.id = r.target_object_id
join configitem c1 on c1.id = r.source_key::int
join configitem_version v1 on v1.configitem_id = c1.id
join configitem c2 on c2.id = r.target_key::int
join configitem_version v2 on v2.configitem_id = c2.id
;