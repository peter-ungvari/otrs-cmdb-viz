#!/bin/bash
set -e

OTRS_DIR=${1:-/opt/otrs}
MODULE_DIR=${2:-$(pwd)}

echo module dir: $MODULE_DIR
echo otrs dir: $OTRS_DIR
echo
echo Deleting symlinks:

cd $MODULE_DIR
find -type f -exec rm -vf $OTRS_DIR/{} \;

perl $OTRS_DIR/bin/otrs.Console.pl Maint::Config::Rebuild
