FROM nginx:stable-alpine-perl

ENV OTRS_VERSION=6.0.26

RUN apk add --no-cache \
  encodings \
  expat \
  fcgi \
  fcgiwrap \
  fontconfig \
  freetype \
  libbz2 \
  libfontenc \
  libgcrypt \
  libgpg-error \
  libidn \
  libpng \
  libxml2 \
  libxslt \
  mkfontdir \
  mkfontscale \
  curl \
  perl \
  perl-appconfig \
  perl-archive-zip \
  perl-canary-stability \
  perl-capture-tiny \
  perl-cgi \
  perl-cgi-fast \
  perl-class-mix \
  perl-common-sense \
  perl-convert-asn1 \
  perl-crypt-eksblowfish \
  perl-crypt-ssleay \
  perl-datetime \
  perl-date-format \
  perl-dbd-pg \
  perl-dbi \
  perl-devel-symdump \
  perl-encode-hanextra \
  perl-encode-locale \
  perl-fcgi \
  perl-file-listing \
  perl-html-parser \
  perl-html-tagset \
  perl-http-cookies \
  perl-http-daemon \
  perl-http-date \
  perl-http-message \
  perl-http-negotiate \
  perl-io-html \
  perl-io-socket-ssl \
  perl-json-xs \
  perl-ldap \
  perl-libwww \
  perl-lwp-mediatypes \
  perl-mail-imapclient \
  perl-net-dns \
  perl-net-http \
  perl-net-libidn \
  perl-net-ssleay \
  perl-params-classify \
  perl-parse-recdescent \
  perl-path-class \
  perl-pod-coverage \
  perl-template-toolkit \
  perl-test-pod \
  perl-text-csv \
  perl-text-csv_xs \
  perl-try-tiny \
  perl-types-serialiser \
  perl-uri \
  perl-www-robotrules \
  perl-xml-libxml \
  perl-xml-libxslt \
  perl-xml-namespacesupport \
  perl-xml-parser \
  perl-xml-sax \
  perl-xml-sax-base \
  perl-yaml-libyaml \
  spawn-fcgi \
  ttf-dejavu \
  postgresql-client

### Install OTRS
RUN mkdir -p /var/lib/otrs && \
  echo "Downloading & Extracting OTRS archive otrs-${OTRS_VERSION}.tar.gz" && \
  curl https://ftp.otrs.org/pub/otrs/otrs-${OTRS_VERSION}.tar.gz | tar xzf - -C /var/lib/otrs --strip 1

### Add User
RUN adduser -D -S -h /var/lib/otrs -s /sbin/nologin -G nginx otrs

### Setup OTRS Directories
RUN mkdir -p /var/lib/otrs/var/run/ /var/lib/otrs/var/tmp && \
  mkdir -p /run/spawn-fcgi

### Set Permissions
RUN /var/lib/otrs/bin/otrs.SetPermissions.pl --otrs-user=otrs --web-group=nginx

### Cleanup
RUN rm -rf /var/cache/apk/*

### Networking Configuration
EXPOSE 80

### Files Add
ADD assets /

RUN chmod +x -R /var/lib/otrs/scripts

ENTRYPOINT sh /var/lib/otrs/scripts/start.sh
