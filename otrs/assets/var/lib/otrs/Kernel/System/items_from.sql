with recursive
node_types as (
	select name
	from general_catalog
	where general_catalog_class = 'ITSM::ConfigItem::Class'
	and name in :node_types -- param: node_types
),
edge_types as (
	select name
	from link_type
	where name not in ('Normal', 'ParentChild')
	and name in :edge_types -- param: edge_types
),
params (start_node, depth) as (
	select :start_node, :depth -- params: start_node, depth
),
graph (source, target) as (
	select ci1.id, ci2.id
	from link_relation r
	join link_type t on r.type_id = t.id
	join link_object o on o.name = 'ITSMConfigItem' 
		and o.id = r.source_object_id 
		and o.id = r.target_object_id
	join configitem ci1 on ci1.id = r.source_key::bigint
	join configitem ci2 on ci2.id = r.target_key::bigint
	join general_catalog gc1 on gc1.id = ci1.class_id
	join general_catalog gc2 on gc2.id = ci2.class_id
	where t.name in (select * from edge_types)
	and gc1.name in (select * from node_types)
	and gc2.name in (select * from node_types)
),
undirected (source, target) as (
	select target, source
	from graph
	union all
	select * from graph
),
step (id, depth, path) as (
	select undirected.target, 1, array[undirected.source]
	from undirected
	where source = (select start_node from params)
	union all
	select undirected.target, step.depth + 1, step.path || undirected.source
	from undirected
	join step on undirected.source = step.id
		and undirected.source <> ALL(path) 
		and step.depth < (select depth from params)
),
node as (
	select distinct id from step
),
node_json (nodes) as (
	select jsonb_agg(
		jsonb_build_object(
			'group', 'nodes', 
			'data', jsonb_build_object(
				'id', node.id,
				'name', v.name,
				'type', gc.name
			)
		)
	)
	from node
	join configitem ci on node.id = ci.id
	join configitem_version v on ci.last_version_id = v.id
	join general_catalog gc on ci.class_id = gc.id
),
edge_json (edges) as (
	select jsonb_agg(
		jsonb_build_object(
			'group', 'edges',
			'data', jsonb_build_object(
				'source', r.source_key::bigint, 
				'target', r.target_key::bigint,
				'type', t.name
			)
		)
	)
	from node source
	cross join node target
	join link_relation r
		on r.source_key::bigint = source.id
		and r.target_key::bigint = target.id
	join link_type t on r.type_id = t.id
)
select nodes || edges as result from node_json, edge_json;
