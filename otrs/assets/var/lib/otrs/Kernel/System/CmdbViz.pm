# --
#
# --

package Kernel::System::CmdbViz;

use strict;
use warnings;

## nofilter(TidyAll::Plugin::OTRS::Migrations::OTRS6::SysConfig)

use Kernel::System::EventHandler;
use Kernel::System::VariableCheck qw(:all);
use Storable;

use vars qw(@ISA);

our @ObjectDependencies = (
    'Kernel::Config',
    'Kernel::System::DB',
    'Kernel::System::Cache',
    'Kernel::System::GeneralCatalog',
    'Kernel::System::LinkObject',
    'Kernel::System::Log',
    'Kernel::System::Main',
    'Kernel::System::Service',
    'Kernel::System::User',
    'Kernel::System::VirtualFS',
    'Kernel::System::XML',
);

=head1 NAME

Kernel::System::CmdbViz - CMDB visualization lib

=head1 DESCRIPTION

CMDB visualization data access functions.

=head1 PUBLIC INTERFACE

=head2 new()

create an object
    use Kernel::System::ObjectManager;
    local $Kernel::OM = Kernel::System::ObjectManager->new();
    my $CmdbVizObject = $Kernel::OM->Get('Kernel::System::CmdbViz');

=cut

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub CmdbTypes {
    my ( $Self, %Param ) = @_;

    open ( cmdbTypesSql, 'cmdb_types.sql' )
    my $sql = <cmdbTypesSql>;

    return $Kernel::OM->Get('Kernel::System::DB')->Prepare( $sql );
}

sub ItemsFrom {
    my ( $Self, %Param ) = @_;

    open ( itemsFromSql, 'items_from.sql' )
    my $sql = <itemsFromSql>;

    return $Kernel::OM->Get('Kernel::System::DB')->Prepare(
        SQL => $sql,
        Bind => (
            ':node_types' => $Param{NodeTypes},
            ':edge_types' => $Param{EdgeTypes},
            ':start_node' => $Param{StartNode},
            ':depth' => $Param{Depth}
        ),
        Limit => 1,
    );
}
