select json_build_object(
	'itemTypes', (
		select json_agg(gc.name)
		from general_catalog gc 
		where general_catalog_class = 'ITSM::ConfigItem::Class'
	),
	'relationTypes', (
		select json_agg(lt.name)
		from link_type lt 
		where lt.name not in ('Normal', 'ParentChild')
	)
) as result;
