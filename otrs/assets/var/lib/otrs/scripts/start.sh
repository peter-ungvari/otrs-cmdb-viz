#!/bin/sh

### Set Defaults
ROOT_PASS=${ROOT_PASS:-"otrs"}
DB_ENGINE=postgresql

### Set Debug Mode
if [ "$DEBUG_MODE" = "TRUE" ] || [ "$DEBUG_MODE" = "true" ]; then
    set -x
fi

### Sanity Test
if [ ! -n "DB_HOST" ]; then
    echo 'ERROR: No Database Host Entered! '
    exit 1
fi

if [ ! -n "DB_NAME" ]; then
    echo 'ERROR: No Database Pass Entered! '
    exit 1
fi

if [ ! -n "DB_USER" ]; then
    echo 'ERROR: No Database User Entered! '
    exit 1
fi

if [ ! -n "DB_PASS" ]; then
    echo 'ERROR: No Database Pass Entered! '
    exit 1
fi

. /var/lib/otrs/scripts/database/util.$DB_ENGINE.sh

### Make sure that DB is accessible
while true; do
  db_alive_check
  if [ $? -eq 0 ]; then
    echo "DB Server Available"
    break
  fi
  echo "DB Server not available. Waiting for it to initialize.."
  sleep 2
done

### Check to see if this is a new install
db_tables_check
if [ $? -ne 0 ]; then
  echo "Starting a clean OTRS installation ready to be configured !!"

#### Populate Database
  echo "Loading default db schema..."
  db_execute /var/lib/otrs/scripts/database/otrs-schema.$DB_ENGINE.sql
  [ $? -ne 0 ] && echo "ERROR: Couldn't load OTRS database schema !!" && exit 1
  echo "Loading initial db inserts..."
  db_execute /var/lib/otrs/scripts/database/otrs-initial_insert.$DB_ENGINE.sql
  [ $? -ne 0 ] && echo "ERROR: Couldn't load OTRS database initial inserts !!" && exit 1
  db_execute /var/lib/otrs/scripts/database/otrs-schema-post.$DB_ENGINE.sql
  [ $? -ne 0 ] && echo "ERROR: Couldn't load OTRS database schema post inserts !!" && exit 1

  ### Set default admin user password
  echo "Setting password for default admin account root@localhost to: $ROOT_PASS"
  su -s /bin/sh otrs -c  "/var/lib/otrs/bin/otrs.Console.pl Admin::User::SetPassword \
  root@localhost ${ROOT_PASS}"

  echo 'Installing OTRS Packages'
  su -s /bin/sh otrs -c '/var/lib/otrs/scripts/addons.sh'

  echo "Creating alternative admin account 'otrs' with password '$ROOT_PASS'"
  su -s /bin/sh otrs -c  "/var/lib/otrs/bin/otrs.Console.pl Admin::User::Add \
  --user-name otrs \
  --first-name Otrs \
  --last-name OTRS \
  --email-address otrs@otrs.org \
  --password ${ROOT_PASS} \
  --group admin \
  --group itsm-configitem"

  ### Set Permissions
  echo 'Setting Permissions'
  /var/lib/otrs/bin/otrs.SetPermissions.pl --otrs-user=otrs --web-group=nginx

  ### Rebuild Configuration
  echo 'Rebuilding Configuration'
  su -s /bin/sh otrs -c "/var/lib/otrs/bin/otrs.Console.pl Maint::Config::Rebuild"

  echo 'Resetting Cache'
  ### Delete Cache
  su -s /bin/sh otrs -c "/var/lib/otrs/bin/otrs.Console.pl Maint::Cache::Delete"

else
  echo "Existing OTRS Installation found..."
fi

### Start OTRS
echo 'Starting OTRS Processes'
su -s /bin/sh otrs -c '/var/lib/otrs/bin/otrs.Daemon.pl start'

### Set Defaults
CGI_TIMEOUT=${CGI_TIMEOUT:-"180"}
UPLOAD_MAX_SIZE=${UPLOAD_MAX_SIZE:-"2G"}

### Adjust NGINX Runtime Variables
sed -i -e "s/<UPLOAD_MAX_SIZE>/$UPLOAD_MAX_SIZE/g" /etc/nginx/nginx.conf
sed -i -e "s/<CGI_TIMEOUT>/$CGI_TIMEOUT/g" /etc/nginx/conf.d/default.conf

echo 'Starting fcgiwrap'
/usr/bin/spawn-fcgi -s /var/lib/otrs/var/run/socket-1 -P /run/spawn-fcgi/otrs-1.pid -u otrs -g nginx -M 660 -n -- /usr/bin/fcgiwrap &

echo 'Starting nginx'
nginx
