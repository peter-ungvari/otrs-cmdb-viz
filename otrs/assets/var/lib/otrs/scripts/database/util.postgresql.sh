export PGHOST=$DB_HOST
export PGDATABASE=$DB_NAME
export PGUSER=$DB_USER
export PGPASSWORD=$DB_PASS

# check if database $DB_NAME is alive
function db_alive_check() {
  pg_isready 2>&1 > /dev/null
}

# check if roles table exists in database $DB_NAME
function db_tables_check() {
  psql $DB_NAME -c 'SELECT * FROM roles;' 2>&1 > /dev/null
}

# run script passed as parameter against database $DB_NAME
function db_execute() {
  psql $DB_NAME < $1 2>&1 > /dev/null
}
