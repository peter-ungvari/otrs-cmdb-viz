# Uncomment Lines to Install Addons

CMD=/var/lib/otrs/bin/otrs.Console.pl

#ITSM
$CMD Admin::Package::Install https://ftp.otrs.org/pub/otrs/itsm/packages6/:GeneralCatalog-6.0.26.opm
$CMD Admin::Package::Install https://ftp.otrs.org/pub/otrs/itsm/packages6/:ImportExport-6.0.26.opm
$CMD Admin::Package::Install https://ftp.otrs.org/pub/otrs/itsm/packages6/:ITSMCore-6.0.26.opm
$CMD Admin::Package::Install https://ftp.otrs.org/pub/otrs/itsm/packages6/:ITSMConfigurationManagement-6.0.26.opm
