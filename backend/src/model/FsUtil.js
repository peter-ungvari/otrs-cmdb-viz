import fs from 'fs'

async function readSql(fileName) {
  return await readFile(`src/sql/${fileName}`)
}

async function readFile(fileName) {
	const fh = await fs.promises.open(fileName)
  try {
    return await fh.readFile('utf8');
  } finally {
    fh.close();
  }
}

export { readSql, readFile }
