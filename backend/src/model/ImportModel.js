import 'core-js/stable'
import 'regenerator-runtime/runtime'
import pool from './Pool'
import { readSql } from './FsUtil'

export default {

  async classIdMap() {
    const query = await readSql('import_class_id_map.sql')
      .then(sql => pool.query(sql))
    const result = {}
    query.rows.forEach(row => result[row.name] = row.id)
    return result
  },

  async definitionIdMap(classIds) {
    const query = await readSql('import_definition_id.sql')
      .then(sql => pool.query(sql, [classIds]))
    const result = {}
    query.rows.forEach(row => result[row.class_id] = row.id)
    return result
  },

  async ciTypeIdMap() {
    const query = await readSql('import_ci_type_id_map.sql')
      .then(sql => pool.query(sql))
    const result = {}
    query.rows.forEach(row => result[row.name] = row.id)
    return result
  },

  async productionDeploymentStateId() {
    const query = await readSql('import_production_deployment_state_id.sql')
      .then(sql => pool.query(sql))
    return query.rows[0].id
  },

  async operationalIncidentStateId() {
    const query = await readSql('import_operational_incident_state_id.sql')
      .then(sql => pool.query(sql))
    return query.rows[0].id
  },

  async userId(userName) {
    const query = await readSql('import_user_id.sql')
      .then(sql => pool.query(sql, [userName]))
    return query.rows[0].id
  },

  async createConfigItem(ci) {
    await readSql('import_insert_ci.sql')
      .then(sql => pool.query(sql, [
        ci.number,
        ci.classId,
        ci.importDateTime,
        ci.userId
      ]))

    await readSql('import_insert_ci_version.sql')
      .then(sql => pool.query(sql, [
        ci.number,
        ci.importDateTime,
        ci.userId,
        ci.name,
        ci.definitionId,
        ci.productionId,
        ci.operationalId
      ]))

    await readSql('import_update_ci_last_version.sql')
      .then(sql => pool.query(sql, [ci.number]))

    await readSql('import_insert_xml_storage.sql')
      .then(sql => pool.query(sql, [ci.classId, ci.number, ci.typeId]))
  },

  async linkTypeIdMap() {
    const query = await readSql('import_link_type_map.sql')
      .then(sql => pool.query(sql))
    const result = {}
    query.rows.forEach(row => result[row.name] = row.id)
    return result
  },

  async linkObjectId() {
    const query = await readSql('import_link_object_id.sql')
      .then(sql => pool.query(sql))
    return query.rows[0].id
  },

  async linkStateId() {
    const query = await readSql('import_link_state_id.sql')
      .then(sql => pool.query(sql))
    return query.rows[0].id
  },

  async createLinkRelation(relation) {
    const query = await readSql('import_insert_link_relation.sql')
      .then(sql => pool.query(sql, [
        relation.sourceKey,
        relation.typeId,
        relation.targetKey,
        relation.linkObjectId,
        relation.validId,
        relation.importDateTime,
        relation.userId
      ]))
  }
}
