import 'core-js/stable'
import 'regenerator-runtime/runtime'
import pool from './Pool'
import { readSql } from './FsUtil'

function getJsonResult(query) {
  return query.rows[0].result
}

export default {
  async cmdbTypes() {
    const query = await readSql('cmdb_types.sql')
      .then(sql => pool.query(sql))
    return getJsonResult(query)
  },
  async ciDetails(id) {
    const query = await readSql('cmdb_ci_details.sql')
      .then(sql => pool.query(sql, [id]))
    return getJsonResult(query)
  },
  async graph(start, depth, categories, relationships) {
    const query = await readSql('cmdb_graph_from.sql')
      .then(sql => pool.query(sql, [start, depth, categories, relationships]))
    return getJsonResult(query)
  }
}
