WITH max_version AS (
	SELECT class_id, MAX(version) as version
	FROM configitem_definition
	WHERE class_id = any ($1::integer[])
	GROUP BY class_id
)
SELECT id, class_id
FROM configitem_definition
JOIN max_version USING (class_id, version);
