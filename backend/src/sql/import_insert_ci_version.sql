INSERT INTO configitem_version
(configitem_id, name, definition_id, depl_state_id, inci_state_id, create_time, create_by)
VALUES (
  (SELECT id FROM configitem WHERE configitem_number = $1),
	$4, $5, $6, $7, $2, $3);
