INSERT INTO link_relation (
  source_key,
  type_id,
  target_key,
  source_object_id,
  target_object_id,
  state_id,
  create_time,
  create_by
)
VALUES (
	(SELECT id::text FROM configitem WHERE configitem_number = $1),
	$2,
	(SELECT id::text FROM configitem WHERE configitem_number = $3),
	$4,
	$4,
	$5,
	$6,
	$7
);
