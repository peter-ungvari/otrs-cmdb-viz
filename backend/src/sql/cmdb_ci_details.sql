SELECT json_build_object(
  'name', v.name,
  'className', gcc.name,
  'type', gct.name,
  'version', (SELECT COUNT(*) FROM configitem_version WHERE configitem_id = $1),
  'createdBy', uc.first_name || ' ' || uc.last_name,
  'createdAt', ci.create_time,
  'modifiedBy', um.first_name || ' ' || um.last_name,
  'modifiedAt', ci.change_time,
  'incidentState', gcis.name,
  'deploymentState', gcds.name
) AS result
FROM configitem ci
  JOIN configitem_version v ON ci.last_version_id = v.id
  JOIN general_catalog gcc ON ci.class_id = gcc.id
  JOIN xml_storage s ON s.xml_key = v.id::text
  JOIN general_catalog gct ON s.xml_content_value::int = gct.id
  JOIN users uc ON ci.create_by = uc.id
  JOIN users um ON ci.change_by = um.id
  JOIN general_catalog gcis ON ci.cur_inci_state_id = gcis.id
  JOIN general_catalog gcds ON ci.cur_depl_state_id = gcds.id
WHERE ci.id = $1
  AND s.xml_content_key = '[1]{''Version''}[1]{''Type''}[1]{''Content''}';
