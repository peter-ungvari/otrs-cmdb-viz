SELECT id, name
FROM general_catalog
WHERE general_catalog_class LIKE 'ITSM::ConfigItem::%::Type'
  AND name IN ('Desktop', 'Laptop', 'Building', 'Floor', 'Room', 'LAN');
