truncate configitem_version cascade;
truncate configitem cascade;
truncate configitem_counter cascade;
truncate configitem_history cascade;
delete from xml_storage where xml_type like 'ITSM::ConfigItem::%';

with link_object_id as (
  select id from link_object where name = 'ITSMConfigItem'
)
delete
  from link_relation
  where source_object_id in (select * from link_object_id)
    and target_object_id in (select * from link_object_id);
