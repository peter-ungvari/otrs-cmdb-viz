UPDATE configitem c
SET
  last_version_id = v.id,
  cur_depl_state_id = v.depl_state_id,
  cur_inci_state_id = v.inci_state_id
FROM configitem_version v
WHERE c.id = v.configitem_id
AND c.configitem_number = $1;
