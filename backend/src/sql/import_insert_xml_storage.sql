WITH version_id AS (
  SELECT v.id AS vid
  FROM configitem c
    JOIN configitem_version v ON c.id = v.configitem_id
  WHERE c.configitem_number = $2
)
INSERT INTO xml_storage (xml_type, xml_key, xml_content_key, xml_content_value)
SELECT 'ITSM::ConfigItem::' || $1, vid, '[1]{''Version''}[1]{''Type''}[1]{''TagKey''}', '[1]{''Version''}[1]{''Type''}[1]'
FROM version_id
UNION ALL
SELECT 'ITSM::ConfigItem::' || $1, vid, '[1]{''Version''}[1]{''Type''}[1]{''Content''}', $3
FROM version_id;
