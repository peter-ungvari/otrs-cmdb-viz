import express from 'express'
import OtrsController from './controller/OtrsController'

const app = express()
const port = 3000
app.use(express.json())

app.get('/', (req, res) => res.send({
  endpoints: [{
    cmdbTypes: 'GET /api/otrs/cmdbTypes',
    details: 'GET /api/otrs/ci/:id/details',
    graph: 'GET /api/otrs/ci/:id/graph'
  }]
}))

app.get('/api/otrs/cmdbTypes', OtrsController.cmdbTypesGet)
app.get('/api/otrs/ci/:id/details', OtrsController.ciDetailsGet)
app.get('/api/otrs/ci/:id/graph', OtrsController.graphGet)

app.listen(port, () => console.log(`app running on port ${port}`))
