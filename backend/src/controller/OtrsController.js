import OtrsModel from '../model/OtrsModel'
import CmdbImportService from '../service/CmdbImportService'

export default {
  async cmdbTypesGet(req, res) {
    await OtrsModel.cmdbTypes().then(data => res.send(data))
  },
  async ciDetailsGet(req, res) {
    await OtrsModel.ciDetails(req.params.id).then(data => res.send(data))
  },
  async graphGet(req, res) {
    const start = parseInt(req.params.id);
    const depth = parseInt(req.query.depth);
    const categories = req.query.categories.split(',');
    const relationships = req.query.relationships.split(',');
    await OtrsModel.graph(start, depth, categories, relationships)
      .then(data => res.send(data))
  },
  async ciImportPut(req, res) {
    await CmdbImportService.ciImport().then(data => res.send(data))
  },
  async linkImportPut(req, res) {
    await CmdbImportService.linkImport().then(data => res.send(data))
  }

}
