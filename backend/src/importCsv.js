import { ciImport, linkImport } from './service/CmdbImportService'

const username='jakab'
const prefix = 'test_ci_'
const ciCsv = 'data/cmdb-test-data - items.csv'
const linkCsv = 'data/cmdb-test-data - relations.csv'

async function importCsv() {
	await ciImport(username, prefix, ciCsv)
  await linkImport(username, prefix, linkCsv)
}

importCsv()
