import ImportModel from '../model/ImportModel'
import { readStream } from './CsvParserService'

async function ciImport(username, prefix, fileName) {
  const classes = await ImportModel.classIdMap()
  const definitionIds = await ImportModel.definitionIdMap(Object.values(classes))
  const ciTypes = await ImportModel.ciTypeIdMap()
  const productionId = await ImportModel.productionDeploymentStateId()
  const operationalId = await ImportModel.operationalIncidentStateId()
  const userId = await ImportModel.userId(username)
  const importDateTime = new Date().toLocaleString()

  function itemToCi(item) {
    const classId = classes[item.className]
    return {
      number: prefix + item.number,
      classId,
      importDateTime,
      userId,
      name: item.name,
      definitionId: definitionIds[classId],
      productionId,
      operationalId,
      typeId: ciTypes[item.type]
    }
  }

  return new Promise((resolve, reject) => {
    const dataStream = readStream(fileName)
    dataStream.on('data', item =>
      ImportModel.createConfigItem(itemToCi(item)))
    dataStream.on('end', () =>  resolve())
    dataStream.on('error', error => reject(error))
  })
}

async function linkImport(username, prefix, fileName) {
  const typeIdMap = await ImportModel.linkTypeIdMap()
  const linkObjectId = await ImportModel.linkObjectId()
  const validId = await ImportModel.linkStateId()
  const userId = await ImportModel.userId(username)
  const importDateTime = new Date().toLocaleString()

  return new Promise((resolve, reject) => {
    const dataStream = readStream(fileName)
    dataStream.on('data', link =>
      ImportModel.createLinkRelation({
        sourceKey: prefix + link.sourceNumber,
        typeId: typeIdMap[link.relation],
        targetKey: prefix + link.targetNumber,
        linkObjectId,
        validId,
        importDateTime,
        userId
      }))
    dataStream.on('end', () =>  resolve())
    dataStream.on('error', error => reject(error))
  })
}

export { ciImport, linkImport }
