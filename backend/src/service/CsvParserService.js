import csv from 'csv-parser'
import fs from 'fs'

function readStream(fileName) {
  return fs.createReadStream(fileName)
    .pipe(csv())
    .on('end', () => {
      console.log(`${fileName} processed`);
    })
}

export { readStream }
