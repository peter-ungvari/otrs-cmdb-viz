import cmdb from "@/mixins/cmdb.js";
import Vue from "vue";

const $state = Vue.observable({
  elements: [],
  ciDetails: [],
  navTree: []
});

const buildNavTree = elements => {
  const cis = elements
    .filter(e => e.group === "nodes")
    .map(node => ({
      id: node.data.id,
      name: node.data.name + ` (${node.data.type})`
    }));

  const ciIds = cis.map(ci => ci.id);

  const includes = elements
    .filter(e => e.group === "edges" && e.data.type === "Includes")
    .map(incl => ({
      source: incl.data.source,
      target: incl.data.target
    }));

  const childrenMap = includes.reduce((map, incl) => {
    if (map[incl.source]) {
      map[incl.source].push(incl.target);
    } else {
      map[incl.source] = [incl.target];
    }
    return map;
  }, {});

  const parentMap = includes.reduce((map, incl) => {
    map[incl.target] = incl.source;
    return map;
  }, {});

  const ciById = cis.reduce((map, ci) => {
    map[ci.id] = ci;
    return map;
  }, {});

  const idTree = {};

  const topLevel = ciIds.filter(id => !parentMap[id]);

  const buildTree = (parent, childIds) => {
    if (!childIds) {
      parent.children = [];
      return;
    }
    parent.children = childIds.map(id => {
      const ci = ciById[id];
      buildTree(ci, childrenMap[id]);
      return ci;
    });
  };

  buildTree(idTree, topLevel);

  return idTree.children;
};

const $actions = {
  findGraph(graphParams) {
    return cmdb.methods.getGraph(graphParams).then(elements => {
      $state.elements = elements;
      $state.navTree = buildNavTree(elements);
    });
  },
  selectCiId(ciId) {
    return cmdb.methods.getCiDetails(ciId).then(ciDetails => {
      $state.ciDetails = Object.keys(ciDetails).map(key => {
        let newKey = key.replace(/([A-Z])/g, " $1");
        newKey = newKey.charAt(0).toUpperCase() + newKey.slice(1);
        return [newKey, ciDetails[key]];
      });
    });
  },
  unselectCiId() {
    $state.ciDetails = [];
  }
};

export default { $state, $actions };
