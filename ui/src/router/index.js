import Vue from "vue";
import VueRouter from "vue-router";
import GraphLayout from "../views/GraphLayout.vue";
import Settings from "../views/Settings.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/settings",
    name: "settings",
    component: Settings
  },
  {
    path: "/graph",
    name: "graph",
    component: GraphLayout
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
