export default {
  methods: {
    async getCmdbTypes() {
      return await fetch("api/otrs/cmdbTypes").then(data => data.json());
    },
    async getCiDetails(id) {
      return await fetch(`api/otrs/ci/${id}/details`).then(data => data.json());
    },
    async getGraph(params) {
      const cj = params.categories.join();
      const rj = params.relationships.join();
      const uri = `api/otrs/ci/${params.start}/graph?depth=${params.depth}&categories=${cj}&relationships=${rj}`;
      console.log(uri);
      return await fetch(encodeURI(uri)).then(data => data.json());
    }
  }
};
