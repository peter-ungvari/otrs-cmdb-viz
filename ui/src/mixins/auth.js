export const auth = {
  methods: {
    async getSessionData(sessionId) {
      if (!sessionId) {
        return null;
      }
      return await fetch(`otrs/session?SessionID=${sessionId}`)
        .then(response => response.json())
        .then(data => data.SessionData)
        .then(data => {
          const props = data.reduce((acc, it) => {
            acc[it.Key] = it.Value;
            return acc;
          }, {});
          return {
            email: props.UserEmail,
            fullName: props.UserFullname
          };
        });
    }
  }
};
