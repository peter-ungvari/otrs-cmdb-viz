module.exports = {
  devServer: {
    proxy: {
      "^/otrs": {
        target: "http://localhost:8090/otrs/nph-genericinterface.pl/Webservice"
      },
      "^/api": {
        target: "http://localhost:3000"
      }
    }
  }
};
